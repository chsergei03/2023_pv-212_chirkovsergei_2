#include "image_viewer.hpp"

int32_t main(int32_t argc, char **argv) {
    Application a(argc, argv);

    MainWindow w;
    w.show();

    return QApplication::exec();
}