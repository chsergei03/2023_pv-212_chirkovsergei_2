#ifndef INC_MAIN_WINDOW_HPP
#define INC_MAIN_WINDOW_HPP

#include "../application/application.hpp"
#include "../image_window/image_window.hpp"

#include "../smart_pointer/smart_pointer.hpp"
#include "../smart_pointer/smart_pointer.cpp"

#include <qapplication.h>

#include <QMainWindow>

#include <QMenuBar>
#include <QMenu>

#include <QListWidget>

#include <fstream>
#include <filesystem>

#include <sstream>

// класс 'список изображений'.
class ImagesList : public QListWidget {
Q_OBJECT

public:

    // конструктор изображений.
    explicit ImagesList(QWidget *parent = nullptr);

private slots:

    // открывает окно с изображением, название которого
    // в данном списке содержится в элементе с индексом
    // index.
    void openImageWindow(QListWidgetItem *item);

private:

    // указатель на вектор со всеми путями до загруженных в
    // приложение изображений.
    SmartPointer<std::vector<std::string>> imagesFilepathsPtr;
};

// класс 'главное окно просмотрщика изображений'.
class MainWindow : public QMainWindow {
Q_OBJECT

public:

    // конструктор класса.
    explicit MainWindow(QWidget *parent = nullptr);
};

#endif