#include "main_window.hpp"

ImagesList::ImagesList(QWidget *parent) :
        QListWidget(parent) {
    auto *a = dynamic_cast<Application *>(qApp);

    imagesFilepathsPtr = SmartPointer(a->getImagesFilepathsVectorAdress());

    setGeometry(IMAGES_LIST_X, IMAGES_LIST_Y,
                IMAGES_LIST_WEIGHT, IMAGES_LIST_HEIGHT);

    setFocusPolicy(Qt::NoFocus);

    std::ifstream in("../data/images_list.txt");

    std::string currentLine;

    if (in.is_open()) {
        while (std::getline(in, currentLine)) {
            std::string filename;

            std::stringstream stringStream(currentLine);

            stringStream >> filename;

            QString itemName = QString::fromStdString(filename);

            addItem(itemName);
        }
    }

    in.close();

    connect(this, SIGNAL(itemDoubleClicked(QListWidgetItem * )),
            this, SLOT(openImageWindow(QListWidgetItem * )));
}

void ImagesList::openImageWindow(QListWidgetItem *item) {
    std::string imageFilepath = (*imagesFilepathsPtr)[this->row(item)];

    auto *iw = new ImageWindow(imageFilepath, nullptr);

    iw->show();
}

MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent) {
    setWindowTitle("image viewer");

    setGeometry(MAIN_WINDOW_X, MAIN_WINDOW_Y,
                MAIN_WINDOW_WEIGHT, MAIN_WINDOW_HEIGHT);

    setWindowFlags(Qt::MSWindowsFixedSizeDialogHint);

    menuBar()->setStyleSheet("QMenuBar{font-size: 14px}");

    QMenu *fileMenu = menuBar()->addMenu("Файл");
    fileMenu->setStyleSheet("QMenu{font-size: 14px}");

    auto *openFileAction = new QAction("Открыть", this);
    fileMenu->addAction(openFileAction);

    auto *imagesList = new ImagesList(this);

    Application *app = dynamic_cast<Application *>(qApp);

    app->setImagesListPtr(imagesList);

    connect(openFileAction, SIGNAL(triggered(bool)),
            qApp, SLOT(loadImage()));
}