#ifndef INC_APPLICATION_HPP
#define INC_APPLICATION_HPP

#include <qlistwidget.h>

#include <QApplication>

#include <QFont>

#include <QFileDialog>
#include <QString>

#include <QMainWindow>

#include <QListWidget>

#include <fstream>
#include <filesystem>

#include <vector>

enum geometryConstants {
    HALF_OF_SCREEN_WEIGHT = 960,
    HALF_OF_SCREEN_HEIGHT = 540,

    MAIN_WINDOW_WEIGHT = 315,
    MAIN_WINDOW_HEIGHT = 340,

    MAIN_WINDOW_X = HALF_OF_SCREEN_WEIGHT - MAIN_WINDOW_WEIGHT / 2,
    MAIN_WINDOW_Y = HALF_OF_SCREEN_HEIGHT - MAIN_WINDOW_HEIGHT / 2,

    IMAGES_LIST_X = 7,
    IMAGES_LIST_Y = 30,

    IMAGES_LIST_WEIGHT = 300,
    IMAGES_LIST_HEIGHT = 300,

    IMAGE_BORDER_VALUE = 10,
    IMAGE_TWO_BORDERS_VALUE = 2 * IMAGE_BORDER_VALUE,

    IMAGE_WEIGHT = 750,
    IMAGE_HEIGHT = 750,

    IMAGE_WINDOW_WEIGHT = IMAGE_WEIGHT + IMAGE_TWO_BORDERS_VALUE,
    IMAGE_WINDOW_HEIGHT = IMAGE_HEIGHT + IMAGE_TWO_BORDERS_VALUE,

    IMAGE_WINDOW_X = HALF_OF_SCREEN_WEIGHT - IMAGE_WINDOW_WEIGHT / 2,
    IMAGE_WINDOW_Y = HALF_OF_SCREEN_HEIGHT - IMAGE_WINDOW_HEIGHT / 2
};

// класс 'приложение просмотрщика изображений'.
class Application : public QApplication {
Q_OBJECT

public:

    // конструктор класса.
    Application(int32_t argc, char **argv);

    // возвращает адрес вектора всех путей до изображений,
    // загруженных в приложение.
    std::vector<std::string> *getImagesFilepathsVectorAdress();

    // устанавливает новый адрес для указателя на список изображений.
    void setImagesListPtr(QListWidget *newImagesListAdress);

private:

    // возвращает значение 'истина', если изображение, путь до которого
    // указан в строке imageFilepath, уже загружено в приложение, в противном
    // случае - 'ложь'.
    bool wasImageLoadedToData(const std::string &imageFilepath) const;

public slots:

    // загружает путь до нового изображения в файл
    // с путями до загруженных в приложение изображений.
    // (для этого открывает диалоговое окно для поиска
    // нового изображения).
    void loadImage();

private:

    // вектор всех путей до изображений, загруженных в приложение.
    std::vector<std::string> imagesFilepaths;

    // указатель на список изображений.
    QListWidget *imagesListPtr = nullptr;
};

#endif