#include "application.hpp"

Application::Application(int32_t argc, char **argv) :
        QApplication(argc, argv) {
    setFont(QFont("golos"));

    std::ifstream in("../data/images_list.txt");

    std::string currentLine;

    if (in.is_open()) {
        while (std::getline(in, currentLine)) {
            std::string filename;
            std::stringstream stringStream(currentLine);
            stringStream >> filename;

            std::string filepath = currentLine;

            auto eraseStart = filepath.find(filename);

            filepath.erase(eraseStart, filename.length() + 1);

            imagesFilepaths.push_back(filepath);
        }
    }

    in.close();
}

std::vector<std::string> *Application::getImagesFilepathsVectorAdress() {
    return &imagesFilepaths;
}

void Application::setImagesListPtr(QListWidget *newImagesListAdress) {
    imagesListPtr = newImagesListAdress;
}

bool Application::wasImageLoadedToData(
        const std::string &imageFilepath) const {
    return std::find(std::begin(imagesFilepaths),
                     std::end(imagesFilepaths),
                     imageFilepath) != std::end(imagesFilepaths);
}

void Application::loadImage() {
    QString filepath = QFileDialog::getOpenFileName(
            nullptr,
            tr("Открыть новое изображение"),
            "C:\\",
            "Изображения (*.png)");

    if (filepath.length() > 0 &&
        !wasImageLoadedToData(filepath.toStdString())) {
        std::string imageListFilepath = "../data/images_list.txt";

        std::ofstream out(imageListFilepath, std::ios::app);

        if (!out.is_open()) {
            std::filesystem::create_directories("../data");

            out.open(imageListFilepath, std::ios::app);
        }

        std::filesystem::path p(filepath.toStdString());

        std::string filename = p.stem().string();

        imagesListPtr->addItem(QString::fromStdString(filename));

        imagesFilepaths.push_back(p.string());

        out << filename << ' ' << p.string() << '\n';

        out.close();
    }
}