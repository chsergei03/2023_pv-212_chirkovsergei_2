add_library(application
        application.cpp)

target_link_libraries(application
        Qt::Core
        Qt::Gui
        Qt::Widgets)