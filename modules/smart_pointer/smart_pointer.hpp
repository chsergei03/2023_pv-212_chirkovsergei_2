#ifndef INC_SMART_POINTER_HPP
#define INC_SMART_POINTER_HPP

// шаблонный класс 'умный указатель'.
template<typename T>
class SmartPointer {
public:

    // конструктор класса по умолчанию.
    SmartPointer() = default;

    // возвращает экземпляр класса, сконструированный
    // по указателю на объект obj произвольного типа.
    SmartPointer(T *obj);

    // возращает результат разименования
    // умного указателя.
    T &operator*();

    // деструктор класса.
    ~SmartPointer();

private:

    // указатель на объект.
    T *object;
};

#endif