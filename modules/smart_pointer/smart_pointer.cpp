#include "smart_pointer.hpp"

template<typename T>
SmartPointer<T>::SmartPointer(T *obj) :
        object(obj) {}

template<typename T>
T &SmartPointer<T>::operator*() {
    return *object;
}

template<typename T>
SmartPointer<T>::~SmartPointer() {}