#ifndef INC_IMAGE_WINDOW_HPP
#define INC_IMAGE_WINDOW_HPP

#include "../application/application.hpp"

#include <QMainWindow>

#include <QLabel>
#include <QPixmap>

#include <QDir>

#include <filesystem>

// класс 'окно с изображением'.
class ImageWindow : public QMainWindow {
Q_OBJECT

public:

    // конструктор класса.
    ImageWindow(const std::string &imageFilepath,
                QWidget *parent = nullptr);
};

#endif