#include "image_window.hpp"

ImageWindow::ImageWindow(const std::string &imageFilepath,
                         QWidget *parent) : QMainWindow(parent) {
    QDir directory(std::filesystem::current_path());

    QString imageRelativeFilepath = directory.relativeFilePath(
            QString::fromStdString(imageFilepath));

    auto *pixmap = new QPixmap;
    pixmap->load(imageRelativeFilepath);

    *pixmap = QPixmap(pixmap->scaled(IMAGE_WEIGHT, IMAGE_HEIGHT,
                                     Qt::KeepAspectRatio,
                                     Qt::SmoothTransformation));

    auto *label = new QLabel(this);
    label->setPixmap(*pixmap);
    label->setGeometry(IMAGE_BORDER_VALUE,
                       IMAGE_BORDER_VALUE,
                       IMAGE_WEIGHT,
                       IMAGE_WEIGHT);

    label->show();

    setGeometry(IMAGE_WINDOW_X, IMAGE_WINDOW_Y,
                IMAGE_WINDOW_WEIGHT,
                IMAGE_WINDOW_HEIGHT);

    setWindowFlags(Qt::MSWindowsFixedSizeDialogHint);
}