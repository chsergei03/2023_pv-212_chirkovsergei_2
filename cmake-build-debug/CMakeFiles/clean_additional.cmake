# Additional clean files
cmake_minimum_required(VERSION 3.16)

if("${CONFIG}" STREQUAL "" OR "${CONFIG}" STREQUAL "Debug")
  file(REMOVE_RECURSE
  "CMakeFiles\\image_viewer_autogen.dir\\AutogenUsed.txt"
  "CMakeFiles\\image_viewer_autogen.dir\\ParseCache.txt"
  "image_viewer_autogen"
  "modules\\application\\CMakeFiles\\application_autogen.dir\\AutogenUsed.txt"
  "modules\\application\\CMakeFiles\\application_autogen.dir\\ParseCache.txt"
  "modules\\application\\application_autogen"
  "modules\\image_window\\CMakeFiles\\image_window_autogen.dir\\AutogenUsed.txt"
  "modules\\image_window\\CMakeFiles\\image_window_autogen.dir\\ParseCache.txt"
  "modules\\image_window\\image_window_autogen"
  "modules\\main_window\\CMakeFiles\\main_window_autogen.dir\\AutogenUsed.txt"
  "modules\\main_window\\CMakeFiles\\main_window_autogen.dir\\ParseCache.txt"
  "modules\\main_window\\main_window_autogen"
  "modules\\smart_pointer\\CMakeFiles\\smart_pointer_autogen.dir\\AutogenUsed.txt"
  "modules\\smart_pointer\\CMakeFiles\\smart_pointer_autogen.dir\\ParseCache.txt"
  "modules\\smart_pointer\\smart_pointer_autogen"
  )
endif()
